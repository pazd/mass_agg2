__author__ = 'Dror Paz, BGU'
CreatedOn = '2018/03/21'

import pandas as pd
import os


def massAggregator(fn, out_fn, masses=2):
    '''
    A script that applies mass-based aggregation on time-based database.
    '''
    # pulses_col = 'Hp (up),1'
    # tot_pulses_col = 'tot_'+pulses_col
    # d_time_col = 'dtime'
    # mon_deltaLC_col = 'delta_LC_monotone'
    # mon_tot_LC_col = 'tot_LC_monotone'
    # l7_col = 'L7'
    pulses_col = 'Hp (C),1'
    tot_pulses_col = 'tot_'+pulses_col
    d_time_col = 'dtime'
    mon_deltaLC_col = 'delta_C_monotone'
    mon_tot_LC_col = 'tot_C_monotone'
    l7_col = 'L7'


    df = pd.read_excel(fn, index_col=0, header=0, skiprows=[1], parse_dates=True)
    columns_list = df.columns.tolist()
    columns_list[2] = mon_tot_LC_col
    df.columns = columns_list

    # set time steps
    df.loc[df.index[0],d_time_col] = 1
    for i in range(1,len(df.index)):
        df.loc[df.index[i],d_time_col] = (df.index[i]-df.index[i-1]).total_seconds()

    # set pulses per time step
    df.loc[:,tot_pulses_col] = df[d_time_col]*pd.to_numeric(df[pulses_col], errors='coerce')

    # set monotone LC deltas
    for i in range(len(df.index)):
        iname = df.index[i]
        if i==0:
            df.loc[iname,mon_deltaLC_col] = 0
        else:
            df.loc[iname,mon_deltaLC_col] = df.loc[iname,mon_tot_LC_col]-df.loc[df.index[i-1],mon_tot_LC_col]
        # print i, iname, df.loc[iname,mon_deltaLC_col]
    df.loc[:,mon_deltaLC_col] = df.loc[:,mon_deltaLC_col].fillna(0)
    print df.head()

    writer = pd.ExcelWriter(out_fn)

    def timeDF_to_massDF(in_gdf):
        print 1,
        names = ['Time', 'time_length', 'LC', 'delta_LC', 'LC_flux', 'L7', 'Hp (up),1 [pulses/s]']
        out_s = pd.Series(index=names, dtype=object)
        out_s[0] = in_gdf.index.max()
        out_s[1] = in_gdf[d_time_col].sum()
        out_s[2] = in_gdf[mon_tot_LC_col].max()
        out_s[3] = in_gdf[mon_deltaLC_col].max()
        out_s[4] = out_s[3] / 0.11 / out_s[1]
        out_s[5] = in_gdf[l7_col].mean()
        out_s[6] = in_gdf[tot_pulses_col].sum() / out_s[1]
        print 2
        return out_s


    for mass in masses:
        # calculate mass sums
        mass_sum_col = '%dkg_sum' % mass
        mass_index_col = '%d_index' % mass

        mass_ser = pd.Series(0., index=df.index,name=mass_sum_col)
        index_ser = pd.Series(0, index=df.index,name=mass_index_col)
        delta_ser = df.loc[:,mon_deltaLC_col].copy()
        print delta_ser
        for i in xrange(1,len(df.index)):
            if mass_ser[i-1] >= mass:
                mass_ser[i] = delta_ser[i]
                index_ser[i] = index_ser[i-1]+1
            else:
                mass_ser[i] = mass_ser[i-1]+delta_ser[i]
                index_ser[i] = index_ser[i-1]
        df.loc[:,mass_sum_col] = mass_ser[:]
        df.loc[:,mass_index_col] = index_ser[:]

        print 4,mass_index_col
        dfg = df.groupby(mass_index_col)
        print 5,'%dkg'%mass
        mass_df = dfg.apply(timeDF_to_massDF)
        print 6, '%dkg'%mass
        mass_df.to_excel(writer, '%dkg'%mass)

    if not os.path.exists(os.path.dirname(out_fn)):
        os.makedirs(os.path.dirname(out_fn))
    df.to_excel(writer, 'time_int')
    writer.save()

if __name__ == '__main__':
    in_path = r'C:\Users\USER\Documents\Article\in_xlsx\C_sampler'
    for f in os.listdir(in_path):
        fn = os.path.join(in_path,f)
        # fn = r'C:\Users\USER\Documents\Article\in_xlsx\2016.02.22.xlsx'
        out_fn = fn.replace('in_xlsx', 'out_xlsx').replace('.xlsx','_out.xlsx')
        print out_fn
        masses = [2,3,4,5]
        massAggregator(fn, out_fn, masses)
